package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"net"
	"todo_services/config"
	"todo_services/storage/postgres"

	pb "todo_services/protos"
)

func main() {
	cfg := config.Load()

	add := cfg.GRPCHost + cfg.GRPCPort
	lis, err := net.Listen("tcp", add)
	if err != nil {
		panic(err)
	}

	pgStore, err := postgres.New(context.Background(), cfg)
	if err != nil {
		panic(err)
	}
	defer pgStore.Close()

	s := grpc.NewServer()

	todoStore := postgres.NewTodoRepo(pgStore.Pool)

	pb.RegisterTodoServiceServer(s, todoStore)

	fmt.Println("listening at", add)
	if err = s.Serve(lis); err != nil {
		panic(err)
	}
}
