package postgres

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"time"
	pb "todo_services/protos"
)

type TodoRepo struct {
	db *pgxpool.Pool
	pb.UnimplementedTodoServiceServer
}

func NewTodoRepo(db *pgxpool.Pool) *TodoRepo {
	return &TodoRepo{
		db: db,
	}
}

func (t *TodoRepo) Create(ctx context.Context, todo *pb.CreateTodoRequest) (*pb.Todo, error) {
	var resp = pb.Todo{}

	fmt.Println("Request is: ", todo)

	id := uuid.New()
	query := `insert into todos (id, name, deadline, status) 
			values ($1, $2, $3, $4)
			returning id, name, status`

	if err := t.db.QueryRow(ctx, query, id.String(), todo.Name, time.Now(), todo.Status).Scan(
		&resp.Id,
		&resp.Name,
		&resp.Status,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (t *TodoRepo) Get(ctx context.Context, req *pb.PrimaryKey) (*pb.Todo, error) {
	return nil, nil
}
